﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelManagement;
using System;


namespace utilities
{
    public class SplashScreen : MonoBehaviour
    {
        private ScreenFader screenFader;
        Canvas canvas;
 
        private void Awake()
        {
            screenFader = GetComponent<ScreenFader>();
            canvas = GetComponent<Canvas>();
        }
        private void Start()
        {
            fadeInOpenning();
        }
        private void Update()
        {
            if (Camera.main != null)
            {
                canvas.worldCamera = Camera.main;
            }
        }
        private void fadeInOpenning()
        {
            StartCoroutine(openGame());
        }

        private IEnumerator openGame()
        {
            screenFader.SetAlpha(0f);
            screenFader.FadeOn();
            //LevelManager.LoadNextLevel();
            yield return new WaitForSeconds(1.8f);
            LevelManager.LoadNextLevel();

        }   
    }
}