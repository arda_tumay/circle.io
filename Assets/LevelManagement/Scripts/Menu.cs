﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SampleGame;

namespace LevelManagement
{

    public abstract class Menu<T> : Menu where T : Menu<T>
    {
        private static T instance;
        public static T Instance { get { return instance; } }

        protected virtual void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = (T)this;
            }
        }

        protected virtual void OnDestroy()
        {
            instance = null;
        }

        public static void Open()
        {
            if(MenuManager.Instance != null && Instance != null)
            {
                MenuManager.Instance.openMenu(Instance);
            }
        }
    }

    [RequireComponent(typeof(Canvas))]
    //Abstract shows  that it is the base class for its subclasses
    public abstract class Menu : MonoBehaviour
    {
   
        //Virtual because it's the only method which is using by other subclasses. It gives you using it in subclasses and overriding option.
        public virtual void OnPressBack()
        {

            if (MenuManager.Instance == null)
            {
                Debug.LogWarning("MENUMANAGER instantiation error: menuManager(back) cannot found!");
            }
            else
            {
                MenuManager.Instance.closeMenu();
            }

        }
    }
}
