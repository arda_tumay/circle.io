﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SampleGame;
using UnityEngine.SceneManagement;
namespace LevelManagement
{
    public class PauseMenu : Menu <PauseMenu>
    {
   

        public void OnResumePressed()
        {
            Time.timeScale = 1;
            base.OnPressBack();
        }

        public void OnRestartPressed()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            base.OnPressBack();
        }

        public void OnMainMenuPressed()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(LevelManager.mainMenuIndex);
            MainMenu.Open();
        }

        public void OnQuitPressed()
        {
            Application.Quit();
        }

    }
}
