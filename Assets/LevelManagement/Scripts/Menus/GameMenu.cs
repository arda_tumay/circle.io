﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelManagement
{
    
    public class GameMenu : Menu<GameMenu>
    {
        Canvas canvas;
        private void Start()
        {
            canvas = GetComponent<Canvas>();
        }
        private void Update()
        {
            if(Camera.main != null)
            {
                canvas.worldCamera = Camera.main;
            }
        }
        public void OnPressPause()
        {
            Time.timeScale = 0;
            PauseMenu.Open();
        }
    }

}
