﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SampleGame;

namespace LevelManagement
{
    public class MainMenu : Menu<MainMenu>
    {

        //we are using generic Menu so we dont need to declare same methods again.

        /*  private static MainMenu instance;
         public static MainMenu Instance { get { return instance; } }
   
        private void Awake()
         {
             if (instance != null)
             {
                 Destroy(gameObject);
             }
             else
             {
                 instance = this;
             }
         }
         private void OnDestroy()
         {
             if (instance == this)
             {
                 instance = null;
             }
         }*/

        public void OnPressPlay()
        {
            if (GameManager.Instance != null)
            {
                LevelManager.LoadNextLevel();
            }
            GameMenu.Open();
        }

        public void OnPressSettings()
        {
            SettingsMenu.Open();
        }

        public void OnPressCredits()
        {
            CreditsMenu.Open();
        }

        public override void OnPressBack()
        {
            // If you want to take the backToMainMenu() classes code you need to begin with base.backToMainMenu();
            // base.backToMainMenu();
            Application.Quit();
        }
    }
}