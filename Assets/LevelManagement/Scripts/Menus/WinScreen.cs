﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SampleGame;

namespace LevelManagement
{
    public class WinScreen : Menu<WinScreen>
    {
        Canvas canvas;
        private void Start()
        {
            canvas = GetComponent<Canvas>();
        }
        private void Update()
        {
            if (Camera.main != null)
            {
                canvas.worldCamera = Camera.main;
            }
        }
        public void OnNextLevelPressed()
        {
            base.OnPressBack();
            LevelManager.LoadNextLevel();
        }
        public void OnRestartPressed()
        {
            base.OnPressBack();
            LevelManager.RestartLevel();
        }
        public void OnMainMenuPressed()
        {
            base.OnPressBack();
            LevelManager.LoadLevel(LevelManager.mainMenuIndex);
        }
    }

}
