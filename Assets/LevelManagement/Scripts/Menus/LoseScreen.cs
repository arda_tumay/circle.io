﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace LevelManagement
{
    public class LoseScreen : Menu<LoseScreen>
    {
        Canvas canvas;
        private void Start()
        {
            canvas = GetComponent<Canvas>();
        }
        private void Update()
        {
            if (Camera.main != null)
            {
                canvas.worldCamera = Camera.main;
            }
        }
        public void OnRestartPressed()
        {
            base.OnPressBack();
            LevelManager.RestartLevel();
        }
        public void OnMainMenuPressed()
        {
            base.OnPressBack();
            LevelManager.LoadLevel(LevelManager.mainMenuIndex);
        }
    }
}