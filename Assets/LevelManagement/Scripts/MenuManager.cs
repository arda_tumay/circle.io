﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

namespace LevelManagement
{
    public class MenuManager : MonoBehaviour
    {
        [Header("MenuPrefabs")]
        [SerializeField] private MainMenu mainMenuPrefab;
        [SerializeField] private CreditsMenu creditsMenuPerfab;
        [SerializeField] private SettingsMenu settingsMenuPerfab;
        [SerializeField] private PauseMenu pauseMenuPrefab;
        [SerializeField] private GameMenu gameMenuPrefab;
        [SerializeField] private WinScreen winScreenPrefab;
        [SerializeField] private LoseScreen loseScreenPrefab;

        [SerializeField] private Transform menuParent;
        [SerializeField] private Stack<Menu> menuStack = new Stack<Menu>();

        //Singleton pattern-1: You must create static instance and make it global with setters and getter.
        private static  MenuManager instance;
        public static MenuManager Instance { get{ return instance; } }

        private void Awake()
        {
            //Singleton pattern-2: You must have only one global instance!
            if (Instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                InitializeMenus();
                //Dont destroy MenuManager after the scene changed
                DontDestroyOnLoad(gameObject);
            }          
        }

        private void OnDestroy()
        {
            if(instance == this)
            {
                instance = null;
            }
        }

        private void InitializeMenus()
        {
            //menu parent is empty game object which is creating for storing other menus
            if(menuParent == null)
            {
                GameObject menuParentObject = new GameObject("Menus");
                menuParent = menuParentObject.transform;
            }
            //Dont destroy the menus 
            DontDestroyOnLoad(menuParent.gameObject);

            //Menu prefabs array
            /*Menu[] menuPrefabs = { mainMenuPrefab, creditsMenuPerfab, settingsMenuPerfab, pauseMenuPrefab, gameMenuPrefab, winScreenPrefab };
            foreach (Menu prefab in menuPrefabs)
            {
                if (prefab != null)
                {
                    Menu menuInstance = Instantiate(prefab, menuParent);
                    if (prefab != mainMenuPrefab)
                    {
                        menuInstance.gameObject.SetActive(false);
                    }
                    else
                    {
                        openMenu(menuInstance);
                    }
                }
            }*/

            //------------------------------------------------------IMPORTANT: Second way-----------------------------------------------------
            //The second way to get variables from the class is using System.Reflection
            //This reflect variables of a class or the inheritance 

            //BindingFlag helps to specify variables such as public or private, Static or non-Static(Instance) etc.
            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;
            //this.GetType() because we want to get MenuManager.class's variables. We wanna multiple fields so we are putting it into array.
            FieldInfo[] fields = this.GetType().GetFields(bindingFlags);
         
            foreach (FieldInfo field in fields)
            {
                Menu prefab = field.GetValue(this) as Menu;
                
                if (prefab != null)
                {
                    Menu menuInstance = Instantiate(prefab, menuParent);
                    if (prefab != mainMenuPrefab)
                    {
                        menuInstance.gameObject.SetActive(false);
                    }
                    else
                    {
                        openMenu(menuInstance);
                    }     
                }
            }
        }
   
        public void openMenu(Menu menuInstance)
        {
            if(menuInstance == null)
            {
                Debug.LogWarning("Menu instantiation error!");
                return;
            }

            if(menuStack.Count > 0)
            {
                foreach (Menu menu in menuStack)
                {
                    menu.gameObject.SetActive(false);
                }
            }

            menuInstance.gameObject.SetActive(true);
            menuStack.Push(menuInstance);
        }
         
        public void closeMenu()
        {
            if(menuStack.Count == 0)
            {
                Debug.LogWarning("MENUMANAGER close menu error: no menus in stack!");
                return;
            }

            Menu topMenu = menuStack.Pop();
            topMenu.gameObject.SetActive(false);

            if(menuStack.Count > 0)
            {
                Menu nextMenu = menuStack.Peek();
                nextMenu.gameObject.SetActive(true);
            }
        }
    }
}

