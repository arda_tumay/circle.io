﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LevelManagement;

namespace SampleGame
{
    public class GameManager : MonoBehaviour
    {
        // reference to player
        // private ThirdPersonCharacter _player;
        private GameObject player;
        // reference to player
        private CircleFactory objective;

        private bool isGameOver;
        public bool IsGameOver { get { return isGameOver; } }

        //Singleton pattern-1: You must create static instance and make it global with setters and getter.
        private static GameManager instance;
        public static GameManager Instance { get { return instance; } }

        // initialize references
        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
            }
            player = GameObject.FindGameObjectWithTag("bomb");

           
        }
      
        private void OnDestroy()
        {
            if (instance == this)
            {
                instance = null;
            }
        }
    
        // check for the end game condition on each frame
        private void Update()
        {

            
            if (objective != null && objective.IsWin())
            {
                Debug.Log("Baba win: " + objective.IsWin());
                PassLevel();
            }
            if (objective != null && objective.IsLose())
            {
                RestartLevel();
            }
        }
        public void RestartLevel()
        {
            Debug.Log("Lose");
            isGameOver = true;
            LoseScreen.Open();          
        }
        // end the level
        public void PassLevel()
        {
            Debug.Log("Win");
            if (player != null)
               {
                   Rigidbody rbody = player.GetComponent<Rigidbody>();
                   if (rbody != null)
                   {
                       rbody.velocity = Vector2.zero;
                   }                
                // force the player to a stand still               
               }

               // check if we have set IsGameOver to true, only run this logic once
               if (!isGameOver)
               {              
                isGameOver = true;
                WinScreen.Open();         
               }
        }      

    }
}