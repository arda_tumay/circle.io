﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circle : MonoBehaviour
{
    bool isFinishedGrowing;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        isFinishedGrowing = false;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.gameObject.tag == "bomb")
        {
            //When circles collide with bomb. Code here.
            GameObject.Find("Backgraund").SendMessage("CircleCollision", other.transform.gameObject.tag);
        }else if (other.transform.gameObject.tag == "shark")
        {
            StartCoroutine(Dest());
        }
    }

    // called when the cube hits the floor
    void OnCollisionEnter2D(Collision2D col)
    {
        
    }

    IEnumerator Dest()
    {
        anim.SetTrigger("isDestroyed");
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);

    }
    void SetIsFinishedGrowing(bool isFinished)
    {
        isFinishedGrowing = isFinished;
    }

    public bool GetIsFinishedGrowing()
    {
        return isFinishedGrowing ;
    }
}
