﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using LevelManagement;

public class CircleFactory : MonoBehaviour
{
    public GameObject circle;
    private GameObject currentInstantiatedCircle;
    private Collider2D[] currentCircleColliders;
    bool mouseLeftPressed, circleInstantiated, circleDestroyedByBomb;

    public int difficultyPercentage;
    float difficultyToPosInCoordinate = 1;
    float circleVolToPosInCoordinate = -1500;
    float totalCircleVolumePercantage = 0;
    float totalCircleArea = 0;
    float GameBackgroundSurfaceArea = 0;
    Vector3 WorldSizeOfBackground;

    int balloonCountInScreen = 0;
    int totalBaloonCount = 0;
    EdgeCollider2D edgeColliderForCircleVolume;
    GameObject Water;
    bool flag, levelSucceeded;

    float waterBarLevel = 0;
    [HideInInspector] // Hides var below
    public bool waterLevelUpdate { get; set; }
    [HideInInspector] // Hides var below
    public bool waterBarLevelUpdate { get; set; }

    [HideInInspector] // Hides var below
    public bool enableMouse { get; set; }//Disable mouse when baloon is hit by shirunken so it can not move on the screen eventhough is hit.

    [HideInInspector] // Hides var below
    public bool checkWinCondition { get; set; }
    
    public SimpleHealthBar healthBar;
    Animator circleAnimator;
    public int baloonLimit;
    public string nextLevel;

    GameObject CircleVolumeObject;

    // Start is called before the first frame update
    void Start()
    {
        if(GameMenu.Instance != null)
        {
            //healthBar = GameMenu.Instance.GetComponentInChildren<SimpleHealthBar>();
        }
        CircleVolumeObject = GameObject.Find("circleVolumeLine");//Empty game object with edge collider to detect baloon collision for water
        GameObject.Find("BalloonCount").GetComponent<Text>().text = "Balloons " + (baloonLimit - totalBaloonCount);
        flag = false;
        waterLevelUpdate = false;
        waterBarLevelUpdate = false;
        levelSucceeded = false;
        Water = GameObject.FindGameObjectWithTag("water");
        mouseLeftPressed = false;
        circleInstantiated = false;//keep track of whether there is a circle currently being blowing up
        currentInstantiatedCircle = null;//keep track of last initiated circle
        circleDestroyedByBomb = false;//if circle is destroyed by the shirunken

        //edge collider for difficulty
        EdgeCollider2D edgeCollider = gameObject.AddComponent<EdgeCollider2D>();
        edgeCollider.isTrigger = true;

        //edge collider for circle volume is being initializing
        InitializeEdgeColliderForCircleVolume();

        //get the size of the bg image
        Vector2 sprite_size = GetComponent<SpriteRenderer>().sprite.rect.size;
        Vector2 local_sprite_size = sprite_size / GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
        Vector3 world_size;//position of bg image in world coordinates
        world_size = local_sprite_size;
        world_size.x *= transform.lossyScale.x;
        world_size.y *= transform.lossyScale.y;
        WorldSizeOfBackground = world_size;

        
        //convert to screen space size
        Vector3 screen_size = 0.5f * world_size / Camera.main.orthographicSize;
        screen_size.y *= Camera.main.aspect;
        //size in pixels
        Vector3 in_pixels = new Vector3(screen_size.x * Camera.main.pixelWidth, screen_size.y * Camera.main.pixelHeight, 0) * 0.5f;
        Debug.Log(string.Format("World size: {0}, Screen size: {1}, Pixel size: {2}", world_size, screen_size, in_pixels));

        //set the initial position of water which is the bottom of the bg image
        Debug.Log(-world_size.y / (2 * gameObject.transform.localScale.y));
        //GameObject.Find("efesikbeni").transform.position = new Vector2(Water.transform.localPosition.x, (-world_size.y / (2) - 0.5f));
        Vector2 waterPostion = new Vector2(Water.transform.position.x, (-world_size.y / (2) - 5));
        Water.transform.position = waterPostion;

        //set the difficulty edge collider's position in accordance with the difficultyPercantage value
        float difficultyToPos = (world_size.y / gameObject.transform.localScale.y) * difficultyPercentage / 100;
        if (difficultyToPos < world_size.y / (2 * gameObject.transform.localScale.y))
        {
            difficultyToPosInCoordinate = -world_size.y / (2 *gameObject.transform.localScale.y) + difficultyToPos;
        }
        else
        {
            difficultyToPosInCoordinate = difficultyToPos;
        }
        Vector2[] edgeColliderPos = new Vector2[2];//set the position of difficulty edge collider
        edgeColliderPos[0] = new Vector2(-world_size.x/(2*gameObject.transform.localScale.x), difficultyToPosInCoordinate);
        edgeColliderPos[1] = new Vector2(world_size.x/(2 * gameObject.transform.localScale.x), difficultyToPosInCoordinate);
        edgeCollider.points = edgeColliderPos;
        GameBackgroundSurfaceArea = world_size.x * world_size.y;
        
    }

    void InitializeEdgeColliderForCircleVolume()
    {
        edgeColliderForCircleVolume = CircleVolumeObject.AddComponent<EdgeCollider2D>();
        edgeColliderForCircleVolume.isTrigger = true;
        Vector2[] edgeColliderPosForCircleVol = new Vector2[2];
        edgeColliderPosForCircleVol[0] = new Vector2(-WorldSizeOfBackground.x / (2 * gameObject.transform.localScale.x), -WorldSizeOfBackground.y / (2 * gameObject.transform.localScale.y));
        edgeColliderPosForCircleVol[1] = new Vector2(WorldSizeOfBackground.x / (2 * gameObject.transform.localScale.x), -WorldSizeOfBackground.y / (2 * gameObject.transform.localScale.y));
        edgeColliderForCircleVolume.points = edgeColliderPosForCircleVol;
    }

    private void FixedUpdate()
    {
        //changeLeve();
        if (waterLevelUpdate)
        {
            UpdateWaterLevel();

        }
        if (waterBarLevelUpdate)
        {
            UpdateWaterBarLevel();
        }
    }

    // Update is called once per frame
    void Update()
    {

        IsWin();
        IsLose();

        if (Input.GetMouseButtonDown(0))
        {
            checkWinCondition = false;
            mouseLeftPressed = !mouseLeftPressed;
            totalBaloonCount++;
            GameObject.Find("BalloonCount").GetComponent<Text>().text = "Balloons " + (baloonLimit - totalBaloonCount);
        }
        if (Input.GetMouseButtonUp(0))
        {
            mouseLeftPressed = !mouseLeftPressed;
            circleInstantiated = !circleInstantiated;
            if (!circleDestroyedByBomb)
            {
                balloonCountInScreen++;
                currentInstantiatedCircle.tag = "finished";
                currentCircleColliders[0].isTrigger = false;
                currentCircleColliders[1].isTrigger = false;
                CalculateCircleArea();//calculate each circle that is not dead by the shirunken
                enableMouse = false;
            }
            else
            {
                enableMouse = false;
                circleDestroyedByBomb = false;
            }
        }
        if (mouseLeftPressed)
        {
            if (!circleInstantiated)
            {
                enableMouse = true;
                InstantiateCircle();
                circleInstantiated = !circleInstantiated;
            }
            if(currentInstantiatedCircle != null)
            {
                ResizeCircle();
                if (enableMouse)
                {
                    ChangeCirclePos();
                }
            }

        }
    }

    public bool IsWin()
    {      
        if (checkWinCondition && !waterLevelUpdate && !waterBarLevelUpdate && difficultyToPosInCoordinate > 0 && (circleVolToPosInCoordinate >= difficultyToPosInCoordinate))
        {//if difficulty value positive for wining cond.
            Debug.Log("bitti 1");
            levelSucceeded = true;
            GameObject.FindGameObjectWithTag("bomb").SendMessage("StopBomb");
            // SceneManager.LoadScene(nextLevel);
            return levelSucceeded;
        }
        if (checkWinCondition && !waterLevelUpdate && !waterBarLevelUpdate && difficultyToPosInCoordinate < 0 && (circleVolToPosInCoordinate >= difficultyToPosInCoordinate))
        {//if difficulty value negative for wining cond.
            Debug.Log("bitti 2");
            Debug.Log(!waterLevelUpdate);
            Debug.Log(!waterBarLevelUpdate);
            levelSucceeded = true;
            GameObject.FindGameObjectWithTag("bomb").SendMessage("StopBomb");
            // SceneManager.LoadScene(nextLevel);
            return levelSucceeded;
        }
        return false;
    }

    public bool IsLose()
    {
        
        if (!levelSucceeded && totalBaloonCount == baloonLimit && !mouseLeftPressed)
        {
            //level failed because balloon limit is exceeded and water level is not enough for success.
            //restart the game or back to main menu
            GameObject.FindGameObjectWithTag("bomb").SendMessage("StopBomb");
            return true;
        }
        return false;
    }

    public bool IsInGameArea()
    {


        return false;
    }

    void changeLeve()
    {
       
        Vector3 bgScale = gameObject.transform.localScale;
        if (bgScale.x > 0.1 && !flag)
        {
            bgScale.x = bgScale.x - 0.01f;
            bgScale.y = bgScale.x - 0.01f;
            bgScale.z = bgScale.x - 0.01f;
            gameObject.transform.localScale = bgScale;
        }else if(bgScale.x <= 0.15)
        {
            // GetComponent<SpriteRenderer>().sprite = 
            flag = true;
        }
        if (flag)
        {
            bgScale.x = bgScale.x +  0.01f;
            bgScale.y = bgScale.x +  0.01f;
            bgScale.z = bgScale.x +  0.01f;
            if (bgScale.x < 1.11)
            {
                gameObject.transform.localScale = bgScale;
            }
            else if (bgScale.x == 1 || bgScale.x > 1)
            {
                flag = false;
            }
        }
    }

    void CalculateCircleArea()
    {//calculate circle area w.r.t radius of its collider. radius must be re-scaled according to scale of the object
        Vector3 scale = currentInstantiatedCircle.transform.localScale;
        float radius = currentInstantiatedCircle.GetComponent<CircleCollider2D>().radius;
        float realRadius = scale.x* radius;
        float area = 2 * Mathf.PI * realRadius * realRadius;
        totalCircleArea = totalCircleArea + area;
        float percentageOfCircle = area / GameBackgroundSurfaceArea * 100;
        totalCircleVolumePercantage = totalCircleVolumePercantage + percentageOfCircle;
        DrawCircleVolumeLine();//update the position of volume line edge collider as new circles are initiated successfuly.
    }

    void DrawCircleVolumeLine()
    {//calculate the percentage of the volume line according the percantage of the circles to the bg image. position line on y axis. x axis is constant.
        float circleVolToPos = WorldSizeOfBackground.y / gameObject.transform.localScale.y * totalCircleVolumePercantage / 100;
        if (circleVolToPos < WorldSizeOfBackground.y / (2 * gameObject.transform.localScale.y))
        {
            circleVolToPosInCoordinate = -WorldSizeOfBackground.y / (2 * gameObject.transform.localScale.y) + circleVolToPos;
        }
        else
        {
            circleVolToPosInCoordinate = circleVolToPos;
        }
        
        Vector2[] edgeColliderPos = new Vector2[2];
        edgeColliderPos[0] = new Vector2(-WorldSizeOfBackground.x/(2 * gameObject.transform.localScale.x), circleVolToPosInCoordinate);
        edgeColliderPos[1] = new Vector2(WorldSizeOfBackground.x/ (2 * gameObject.transform.localScale.x), circleVolToPosInCoordinate);
        edgeColliderForCircleVolume.points = edgeColliderPos;//update edge colliders pos
        //waterLevelUpdate = true;
        //waterBarLevelUpdate = true;
    }


    void UpdateWaterBarLevel()
    {
        waterBarLevel = waterBarLevel + Time.deltaTime * 3f;
        if (waterBarLevel <= totalCircleArea)
        {
            float targetWaterVolume = GameBackgroundSurfaceArea * difficultyPercentage / 100;
            healthBar.UpdateBar(waterBarLevel, targetWaterVolume);
        }
        else
        {
            float targetWaterVolume = GameBackgroundSurfaceArea * difficultyPercentage / 100;
            healthBar.UpdateBar(waterBarLevel, targetWaterVolume);
            waterBarLevelUpdate = false;
            checkWinCondition = true;
        }


    }

    void UpdateWaterLevel()
    {       
        float y = Water.transform.localPosition.y + Time.deltaTime * 0.4f;
        if (circleVolToPosInCoordinate < 0 && ((y ) < circleVolToPosInCoordinate * gameObject.transform.localScale.y - 5))
        {
            Vector2 waterPostion = new Vector2(Water.transform.localPosition.x, y);//update water image pos
            Water.transform.localPosition = waterPostion;
        }
        else if (circleVolToPosInCoordinate > 0 && ((y ) > circleVolToPosInCoordinate * gameObject.transform.localScale.y - 5))
        {
            Vector2 waterPostion = new Vector2(Water.transform.localPosition.x, y);//update water image pos
            Water.transform.localPosition = waterPostion;
        }
        else
        {
            Debug.Log("su bitti");
            Vector2 waterPostion = new Vector2(Water.transform.localPosition.x, y);//update water image pos
            Water.transform.localPosition = waterPostion;
            waterLevelUpdate = false;
            checkWinCondition = true;
        }
        
    }

    void InstantiateCircle()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 1.0f;
        Vector3 mousePositionInWorld = Camera.main.ScreenToWorldPoint(mousePos);
        currentInstantiatedCircle = Instantiate(circle, mousePositionInWorld, circle.transform.rotation);
        circleAnimator = currentInstantiatedCircle.GetComponent<Animator>();
        currentCircleColliders = currentInstantiatedCircle.GetComponents<Collider2D>();
        currentCircleColliders[0].isTrigger = true;
        currentCircleColliders[1].isTrigger = true;
    }

    void ResizeCircle()
    {
        Vector3 circleScale = currentInstantiatedCircle.transform.localScale;
        circleScale.x = circleScale.x + 0.001f;
        circleScale.y = circleScale.x  + 0.001f;
        circleScale.z = circleScale.x  + 0.001f;
        currentInstantiatedCircle.transform.localScale = circleScale;
    }

    void ChangeCirclePos()
    {

        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 1.0f;
        Vector3 mousePositionInWorld = Camera.main.ScreenToWorldPoint(mousePos);
        currentInstantiatedCircle.transform.localPosition = mousePositionInWorld;
    }

    void CircleCollision(string collidedObject)
    {
        StartCoroutine(startCircleDestroyAnim());
        enableMouse = false;
        currentCircleColliders = null;
        if(collidedObject == "bomb")
        {
            circleDestroyedByBomb = true;
        }
    }
    IEnumerator startCircleDestroyAnim()
    {
        circleAnimator.SetTrigger("isDestroyed");
        currentInstantiatedCircle.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
        //Destroy(currentInstantiatedCircle, circleAnimator.GetCurrentAnimatorStateInfo(0).length);
        yield return new WaitForSeconds(1.0f);
        Destroy(currentInstantiatedCircle);
        currentInstantiatedCircle = null;
        circleAnimator = null;

    }

    public int getBaloonCount()
    {
        return balloonCountInScreen;
    }

    public int getTotalBaloonCount()
    {
        return totalBaloonCount;
    }

}
