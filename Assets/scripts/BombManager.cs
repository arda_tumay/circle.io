﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//pozisyon değişmediğinde kuvvet uygula
//birbiri içinden geçmesi için colliderları trigger yap ve çarpışmalara raycast le bak
public class BombManager : MonoBehaviour
{
    Rigidbody2D rb;
    float speed, initialSpeed;
    Vector3 target;
     Vector3 lastFrameVelocity;
    float minVelocity = 10f;
    Vector3 lastPos;
    float threshold = 2.0f; // minimum displacement to recognize a 
    // Start is called before the first frame update
    float oldVelocityX, oldVelocityY, countX = 0f, countY = 0f;
    bool isVelocityChanged = false, bombStopped = false;
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        speed =  500.0f;
        initialSpeed = speed;
        //Vector2 vec = new Vector2(Random.Range(1, 100), Random.Range(1, 100));
        //vec = vec.normalized;
        rb.AddForce(Random.insideUnitCircle.normalized * speed);
        lastPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //float step = speed * Time.deltaTime; // calculate distance to move
        //transform.position = Vector2.MoveTowards(transform.position, target, speed);
        if (!bombStopped)
        {
            transform.Rotate(0, 0, 500 * Time.deltaTime);
        }

        if ((rb.velocity.x == 0 || rb.velocity.y == 0) && !bombStopped)
        {
            rb.AddForce(Random.insideUnitCircle * speed);
            lastFrameVelocity = rb.velocity;
        }
        else
        {
            lastFrameVelocity = rb.velocity;
            
        }
        //Debug.Log(rb.velocity);
        if (!bombStopped && isVelocityChanged)
        {
            oldVelocityX = rb.velocity.x;
            oldVelocityY = rb.velocity.y;
            isVelocityChanged = false;
        }
        
        if (Mathf.Abs(rb.velocity.x) == Mathf.Abs(oldVelocityX))
        {
            countX = countX + Time.deltaTime;
            //rb.AddForce(Random.insideUnitCircle.normalized * speed);
            Debug.Log("velocity x aynı");

        }
        if (Mathf.Abs(rb.velocity.y) == Mathf.Abs(oldVelocityY))
        {
            countY = countY + Time.deltaTime;
            //rb.AddForce(Random.insideUnitCircle.normalized * speed);
            Debug.Log("velocity y aynı");
        }
        if (countX > 5f || countY > 5f)
        {
            Debug.Log("KUVVET UYGULA");
            rb.AddForce(new Vector2(0,0));
            rb.AddForce(Random.insideUnitCircle.normalized * 100);
            //rb.velocity = Random.insideUnitCircle.normalized * Mathf.Max(speed, minVelocity);
            countY = 0; countX = 0;
        }
        /*Vector3 offset = transform.position - lastPos;
        if (offset.x > threshold)
        {
            lastPos = transform.position; // update lastPos
                                    // code to execute when X is getting bigger
        }
        else
        if (offset.x < -threshold)
        {
            lastPos = transform.position; // update lastPos
                                          // code to execute when X is getting smaller 
            var speed = lastFrameVelocity.magnitude;
            rb.velocity = Random.insideUnitCircle.normalized * Mathf.Max(speed, minVelocity);
        }*/

    }

    void OnCollisionEnter2D(Collision2D collision)
    {

        if ((collision.transform.gameObject.tag == "Backgraund" || collision.transform.gameObject.name == "Backgraund") && !bombStopped)
        {
            var speed = lastFrameVelocity.magnitude;
            var direction = Vector3.Reflect(lastFrameVelocity.normalized, collision.contacts[0].normal);
            rb.velocity = direction * Mathf.Max(speed, minVelocity);
            isVelocityChanged = true;
            //rb.velocity = Vector2.Reflect(rb.velocity, collision.contacts[0].normal);
            // rb.AddForce((Random.insideUnitCircle).normalized * speed);
        }
        if (collision.transform.gameObject.tag == "finished" && !bombStopped) {
            //rb.AddForce(Random.insideUnitCircle.normalized * initialSpeed);
            var speed = lastFrameVelocity.magnitude;
            var direction = Vector3.Reflect(lastFrameVelocity.normalized, collision.contacts[0].normal);
            rb.velocity = direction * Mathf.Max(speed, minVelocity);
            isVelocityChanged = true;
        }
        if (collision.transform.gameObject.tag == "circle")
        {
            
        }
        if (collision.gameObject.tag == "bomb")
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }


    void StopBomb()
    {
        rb.velocity = new Vector2(0,0);
        transform.rotation = new Quaternion(0,0,0, 0);
        lastFrameVelocity = new Vector3(0, 0, 0);
        bombStopped = true;
    }

}
